//
//  ProductSearchMLSearchMainViewController.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 15/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import Foundation
import UIKit

class ProductSearchMLSearchMainViewController:UITableViewController, ProductSearchMLSearchMainViewDelegate, UISearchResultsUpdating{
    
    var searchController:UISearchController?

    var searchPresenter:ProductSearchMLSearchPresenter?
    
    let cellIdentifier = "ProductCell"
    
    init() {
        super.init(style: .plain) //TODO learn about initializers
        searchPresenter = ProductSearchMLSearchPresenter(delegate: self)
        searchController = UISearchController(searchResultsController: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        configureTableView()
    }
    
    func configureTableView() {
        
     //   definesPresentationContext = true

        tableView.delegate = self
        tableView.dataSource = self

        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.searchResultsUpdater = self as UISearchResultsUpdating
       // searchController?.dimsBackgroundDuringPresentation = false

        tableView.tableHeaderView = searchController?.searchBar

        searchController?.searchBar.barTintColor = .yellow

        searchController?.searchBar.placeholder = "Buscar en Mercado Libre"

        navigationItem.title = "Busqueda de Productos"

        if let bar:UINavigationBar = navigationController?.navigationBar {
            
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor:UIColor.lightGray]
           
            bar.barTintColor = .yellow
        }

        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
        
    }

    func updateSearchResults(for searchController: UISearchController) {

        let searchText = searchController.searchBar.text
        
        if searchText?.count ?? 0 > 2 { //TODO check if default is right
            searchPresenter?.searchForProducts(withQuery: searchText ?? "") //TODO check defaultValue
        }
    }

    func reloadResults(){
        tableView.reloadData();
    }

    func navigateToProductDetails(withSelectedProduct selectedProduct: Any) {
        
        //TODO check if shoud override        
        //    ProductSearchMLProductDetailViewController *viewController = [[ProductSearchMLProductDetailViewController alloc] initWithProduct:selectedProduct];
        //
        //    [self.navigationController pushViewController:viewController animated:true];
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ProductSearchMLProductUITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ProductSearchMLProductUITableViewCell ?? ProductSearchMLProductUITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: cellIdentifier)
        
        searchPresenter?.renderCell(withRow:indexPath.row, andCellViewDelegate:cell)
        
        return cell;
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        searchPresenter?.navigateToProductDetail(selectedRow: indexPath.row);
    
        //    NSLog(@"title of cell %@", [[self.searchPresenter getProducts] objectAtIndex:indexPath.row]);
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchPresenter?.getResultsCount() ?? 0
    }

//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 800
//    }
 
}
