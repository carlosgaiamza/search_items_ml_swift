//
//  TableViewCell.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 18/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import UIKit
import AFNetworking.UIImageView_AFNetworking
import PureLayout

class ProductSearchMLProductUITableViewCell: UITableViewCell, ProductSearchMLProductUITableViewCellDelegate {
    
    var customImageView:UIImageView = UIImageView(frame: CGRect.zero)
    
    let titleLabel:UILabel = {
        
        let titleLabel:UILabel = UILabel(frame: .zero)
        
        titleLabel.textColor = .gray
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: "Arial", size: 16.0)
        
        return titleLabel
    }()
    
    let priceLabel:UILabel = {

        let priceLabel:UILabel = UILabel(frame: CGRect.zero)

        priceLabel.textColor = UIColor.black
        priceLabel.textAlignment = NSTextAlignment.right
        priceLabel.font = UIFont(name: "Arial", size: 16.0)

        return priceLabel
    }()
    
    let DEFAULT_MARGIN:CGFloat = 10
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // configure control(s)
        addSubview(titleLabel)
        addSubview(priceLabel)
        addSubview(customImageView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
      
        self.clipsToBounds = true
        
        // Layout
        customImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        
        customImageView.autoPinEdge(.top, to: .top, of: self)
        customImageView.autoPinEdge(.leading, to: .leading, of: self, withOffset: DEFAULT_MARGIN)
        customImageView.autoPinEdge(.bottom, to: .bottom, of: self)
        
        customImageView.autoSetDimension(.height, toSize: self.bounds.width/2)
        customImageView.autoSetDimension(.width, toSize: self.bounds.width/2)
        
        titleLabel.autoPinEdge(.top, to: .top, of: self, withOffset: DEFAULT_MARGIN)
        titleLabel.autoPinEdge(.leading, to: .trailing, of: customImageView, withOffset: DEFAULT_MARGIN)
        titleLabel.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -DEFAULT_MARGIN)
        
        priceLabel.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: DEFAULT_MARGIN)
        priceLabel.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -DEFAULT_MARGIN)
        priceLabel.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -DEFAULT_MARGIN)
      
        //  let c1 = priceLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
//        c1.priority = UILayoutPriority.defaultLow
//        c1.isActive = true
        super.updateConstraints()
    }
    
    func setTitle(title: String, andPrice price: NSNumber, andThumbnail thumbnail: String) {
        
        titleLabel.text = title
        
        priceLabel.text = "$ \(price)"
        
        let url1 = thumbnail
        let placeholderImage = UIImage(named:"no-product-image")
        
        let urlRequest:URLRequest = URLRequest(url: URL(string: url1)!) //TODO remove ignore
        
        customImageView.setImageWith(urlRequest, placeholderImage: placeholderImage,
                                               success: { [weak customImageView] (request:URLRequest!,response:HTTPURLResponse!, image:UIImage!) -> Void in
                                                
                                                //TODO check how to work with weak, block and closures
                                                self.setNeedsLayout()
                                                self.customImageView.clipsToBounds = true
                                                self.customImageView.contentMode = UIView.ContentMode.scaleAspectFit
                                                self.customImageView.bounds = CGRect.zero
                                                self.customImageView.image = image
                                                self.updateConstraints()

        }, failure: {(request:URLRequest!,response:HTTPURLResponse!, error:Error!) -> Void in
               // NSLog("%@", error) TODO add logs
        })
       
    }
    
}
