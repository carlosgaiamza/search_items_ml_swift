//
//  ProductSearchMLProductUITableViewCellDelegate.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 18/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import Foundation

protocol ProductSearchMLProductUITableViewCellDelegate{
    
    func setTitle(title:String, andPrice price:NSNumber, andThumbnail thumbnail:String)
    
}
