//
//  ProductSearchMLSearchMainViewDelegate.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 18/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import Foundation

protocol ProductSearchMLSearchMainViewDelegate {

    func reloadResults()
    func navigateToProductDetails(withSelectedProduct selectedProduct:Any)

}
