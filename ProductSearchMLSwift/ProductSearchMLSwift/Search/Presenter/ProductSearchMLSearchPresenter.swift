//
//  ProductSearchMLSearchPresenter.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 15/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import Foundation
import Mantle

class ProductSearchMLSearchPresenter{
    
    var searchProvider:ProductSearchMLSearchProvider = ProductSearchMLSearchProvider()
    var mainViewDelegate:ProductSearchMLSearchMainViewDelegate? //TODO, check dependency type weak
    
    lazy var content:NSArray = []
    
    init(delegate: ProductSearchMLSearchMainViewDelegate){

          mainViewDelegate = delegate
    }
    
    func getProducts() -> NSArray {
        return content
    }
    
    func searchForProducts( withQuery query: String) {
        
        if !query.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).isEmpty {
            
            searchProvider.searchProduct(withQuery:query,
                                         success:{(task: URLSessionTask?, responseObject: Any?) in
                
                                            do {
                                                
                                                let productos = try MTLJSONAdapter.models(of: ProductSearchMLProductDTO.self, fromJSONArray: (responseObject  as! NSDictionary).object(forKey:"results") as? [Any])
                                            
                                                self.content = productos as NSArray
                                                
                                                self.mainViewDelegate?.reloadResults()

                                            } catch {
                                                print(error.localizedDescription)
                                            }
                                            
            }, failure: {(URLSessionTask, NSError) in
 
                self.content = []
                self.mainViewDelegate!.reloadResults()
            })
            
        } else {
            
            self.content = []
            mainViewDelegate?.reloadResults()
        }
        
    }

    func getResultsCount() -> Int{
        
        return content.count
    }

    func navigateToProductDetail(selectedRow:NSInteger){

        mainViewDelegate?.navigateToProductDetails(withSelectedProduct: content.object(at: selectedRow))
    }

    func renderCell(withRow selectedRow: NSInteger, andCellViewDelegate cellView: ProductSearchMLProductUITableViewCellDelegate) {

        let product:ProductSearchMLProductDTO = content.object(at: selectedRow) as! ProductSearchMLProductDTO;

        //TODO manage defaults
        cellView.setTitle(title: product.title ?? "", andPrice: product.price ?? 0, andThumbnail: product.thumbnail ?? "")
    }
}
