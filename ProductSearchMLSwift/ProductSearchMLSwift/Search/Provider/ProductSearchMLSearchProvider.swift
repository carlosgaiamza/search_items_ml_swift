//
//  ProductSearchMLSearchProvider.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 19/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import Foundation
import AFNetworking

class ProductSearchMLSearchProvider {
    
    let BaseURLString = "https://api.mercadolibre.com/sites/MLU"
    let SearchEndpoint = "search?q="

    func searchProduct(withQuery query:String, success: ((_ task: URLSessionDataTask, _ responseObject: Any?) -> Void)? = nil, failure: ((_ task: URLSessionDataTask?, _ error: Error) -> Void)? = nil) { //TODO reinforce closures definitions
        
        let manager   = AFHTTPSessionManager(baseURL:URL(string: BaseURLString));

        manager.get(SearchEndpoint + query,
                    parameters: nil,
                    progress: nil,
                    success: success,
                    failure: failure)

    }

}
