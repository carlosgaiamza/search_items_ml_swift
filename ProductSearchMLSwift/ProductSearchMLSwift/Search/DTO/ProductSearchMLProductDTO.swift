//
//  ProductSearchMLProductDTO.swift
//  ProductSearchMLSwift
//
//  Created by CARLOS GAIA on 19/10/2018.
//  Copyright © 2018 CARLOS GAIA. All rights reserved.
//

import Foundation
import Mantle

class ProductSearchMLProductDTO:  MTLModel, MTLJSONSerializing {
    
    @objc private(set) var title: String?;
    @objc private(set) var price: NSNumber?;
    @objc private(set) var thumbnail: String?;
    @objc private(set) var currencyId: String?;
    @objc private(set) var soldQuantity: NSNumber?;
    @objc private(set) var availableQuantity: NSNumber?;
    @objc private(set) var condition: String?;

    class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
    
        return ["title": "title",
                "price": "price",
                "thumbnail": "thumbnail",
                "currencyId": "currency_id",
                "availableQuantity": "available_quantity",
                "soldQuantity": "sold_quantity",
                "condition": "condition"]

    }
}
