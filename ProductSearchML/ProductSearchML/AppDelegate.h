//
//  AppDelegate.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProductSearchMLSearchMainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ProductSearchMLSearchMainViewController *viewController;

@end

