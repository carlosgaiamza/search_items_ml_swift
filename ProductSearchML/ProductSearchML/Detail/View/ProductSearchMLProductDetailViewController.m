//
//  ProductSearchMLProductDetailViewController.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//
#define MAS_SHORTHAND

#import "ProductSearchMLProductDetailViewController.h"
#import "ProductSearchMLProductUITableViewCell.h"
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"
#import "ProductSearchMLProductDetailPresenter.h"

@interface ProductSearchMLProductDetailViewController ()
     @property (strong,nonatomic) ProductSearchMLProductDetailPresenter *productDetailPresenter;
@end

const int DETAIL_MARGIN = 20;

@implementation ProductSearchMLProductDetailViewController

- (instancetype)initWithProduct:(ProductSearchMLProductDTO *)product{
    self = [super init];
    
    if (self) {
        _productDetailPresenter = [[ProductSearchMLProductDetailPresenter alloc] initWithProduct:product andViewDelegate:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self configureView];
    
    [self.productDetailPresenter updateViewWithProductInfo];
    
    [self updateConstraints];
}
    
-(void)configureView
{
    // configure control(s)
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.textColor = [UIColor darkGrayColor];
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _titleLabel.numberOfLines = 0;
    _titleLabel.font = [UIFont fontWithName:@"Arial" size:26.0f];
    [self.view addSubview:_titleLabel];
    
    _priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _priceLabel.textColor = [UIColor darkGrayColor];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.font = [UIFont fontWithName:@"Arial" size:26.0f];
    [self.view addSubview:_priceLabel];
   
    _conditionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _conditionLabel.textColor = [UIColor grayColor];
    _conditionLabel.textAlignment = NSTextAlignmentRight;
    _conditionLabel.font = [UIFont fontWithName:@"Arial" size:10.0f];
    [self.view addSubview:_conditionLabel];

    _conditionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _conditionLabel.textColor = [UIColor darkGrayColor];
    _conditionLabel.font = [UIFont fontWithName:@"Arial" size:16.0f];
    [self.view addSubview:_conditionLabel];

    _soldQuantityLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _soldQuantityLabel.textColor = [UIColor darkGrayColor];
    _soldQuantityLabel.textAlignment = NSTextAlignmentRight;
    _soldQuantityLabel.font = [UIFont fontWithName:@"Arial" size:16.0f];
    [self.view addSubview:_soldQuantityLabel];
    
    _availableQuantityLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _availableQuantityLabel.textColor = [UIColor darkGrayColor];
    _availableQuantityLabel.textAlignment = NSTextAlignmentRight;
    _availableQuantityLabel.font = [UIFont fontWithName:@"Arial" size:16.0f];
    [self.view addSubview:_availableQuantityLabel];
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [_imageView setClipsToBounds:YES];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_imageView];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.93f green:0.93f blue:0.93f alpha:1.0f];
}
    
- (void)updateConstraints {
    
    [self.imageView remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.height.equalTo(self.view.mas_height).dividedBy(3);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view.mas_left);
    }];
    
    [self.conditionLabel remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.top.equalTo(self.imageView.mas_bottom).offset(DETAIL_MARGIN);
        make.left.equalTo(self.view.mas_left).offset(DETAIL_MARGIN);
        make.right.equalTo(self.view.mas_right).offset(-DETAIL_MARGIN);
    }];
    
    [self.titleLabel remakeConstraints:^(MASConstraintMaker *make) {
       // make.width.equalTo(self.view.mas_width);
        make.top.equalTo(self.conditionLabel.mas_bottom).offset(DETAIL_MARGIN);
        make.right.equalTo(self.view.mas_right).offset(-DETAIL_MARGIN);
        make.left.equalTo(self.view.mas_left).offset(DETAIL_MARGIN);
    }];
    
    [self.priceLabel remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(DETAIL_MARGIN);
        make.right.equalTo(self.view.mas_right).offset(-DETAIL_MARGIN);
    }];
    
    [self.availableQuantityLabel remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.top.equalTo(self.priceLabel.mas_bottom).offset(DETAIL_MARGIN);
        make.right.equalTo(self.view.mas_right).offset(-DETAIL_MARGIN);
    }];
    
    [self.soldQuantityLabel remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.top.equalTo(self.availableQuantityLabel.mas_bottom);
        make.right.equalTo(self.view.mas_right).offset(-DETAIL_MARGIN);
    }];
   
}
    
- (void)setTitle:(NSString *)title andPrice:(NSNumber *)price andThumbnail:(NSString *)thumbnail andCurrentyId:(NSString *)currencyId andSoldQuantity:(NSNumber *)soldQuantity andAvailableQuantity:(NSNumber *)availableQuantity andCondition:(NSString *)condition{
    
    self.conditionLabel.text = [NSString stringWithFormat:@"%@", condition];
    
    self.titleLabel.text = title;
    
    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@", @"$", price];
    
    self.soldQuantityLabel.text = [NSString stringWithFormat:@"%@ vendidos", soldQuantity];
  
    self.availableQuantityLabel.text = [NSString stringWithFormat:@"%@ unidads disponibles", availableQuantity];

    NSString *url = thumbnail;
    
    NSURL *parsedURL = [NSURL URLWithString:url];
    
    if (parsedURL && parsedURL.scheme && parsedURL.host) {
        
        UIImage *placeholderImage = [UIImage imageNamed:@"no-product-image"];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:parsedURL];
        
        __weak ProductSearchMLProductDetailViewController *weakSelf = self;
        
        [self.imageView setImageWithURLRequest:urlRequest placeholderImage:placeholderImage success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
         
            weakSelf.imageView.clipsToBounds = YES;

            weakSelf.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [weakSelf.imageView setBounds:CGRectZero];
            [weakSelf.imageView setImage:image];
            [weakSelf.imageView setBackgroundColor:[UIColor whiteColor]];
            [weakSelf updateConstraints];
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            NSLog(@"%@", error);
        }];
    }else{
        NSLog(@"Bad URL Error: %@", url);
    }
}
    
@end
