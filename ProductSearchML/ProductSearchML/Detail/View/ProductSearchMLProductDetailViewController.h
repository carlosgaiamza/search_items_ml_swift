//
//  ProductSearchMLProductDetailViewController.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductSearchMLProductDetailViewDelegate.h"
#import "ProductSearchMLProductDTO.h"

@interface ProductSearchMLProductDetailViewController : UIViewController<ProductSearchMLProductDetailViewDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *soldQuantityLabel;
@property (nonatomic, strong) UILabel *availableQuantityLabel;
@property (nonatomic, strong) UILabel *conditionLabel;
    
- (instancetype)initWithProduct:(ProductSearchMLProductDTO *)product;
    
@end
