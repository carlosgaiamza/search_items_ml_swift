//
//  ProductSearchMLProductDetailPresenter.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 15/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import "ProductSearchMLProductDetailPresenter.h"

@interface ProductSearchMLProductDetailPresenter()
    
    @property (weak,nonatomic) id<ProductSearchMLProductDetailViewDelegate> productDetailViewDelegate;
    
    @property (strong,nonatomic) ProductSearchMLProductDTO *product;
    
@end

@implementation ProductSearchMLProductDetailPresenter

- (instancetype)initWithProduct:(ProductSearchMLProductDTO *)productDetail andViewDelegate:(id<ProductSearchMLProductDetailViewDelegate>)viewDelegate{
   
    self = [super init];
    
    if (self) {
        _product = productDetail;
        _productDetailViewDelegate = viewDelegate;
    }
    
    return self;
}
    
- (void)updateViewWithProductInfo{
    
    [self.productDetailViewDelegate setTitle:self.product.title
                                    andPrice:self.product.price
                                andThumbnail:self.product.thumbnail
                               andCurrentyId:self.product.currencyId
                             andSoldQuantity:self.product.soldQuantity
                        andAvailableQuantity:self.product.availableQuantity
                                andCondition:self.product.condition];
}
    
@end
