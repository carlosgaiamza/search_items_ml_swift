//
//  ProductSearchMLSearchService.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "ProductSearchMLSearchProvider.h"
#import "ProductSearchMLProductDTO.h"

@interface ProductSearchMLSearchProvider()

@end

static NSString * const BaseURLString = @"https://api.mercadolibre.com/sites/MLU/";
static NSString * const SearchEndpoint = @"%@search?q=%@";

@implementation ProductSearchMLSearchProvider

- (void)searchProductWithQuery:(NSString *)query success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure{
    // 1
    NSString *string = [NSString stringWithFormat:SearchEndpoint, BaseURLString, query];
    NSURL *url = [NSURL URLWithString:string];
    
    AFHTTPSessionManager *manager   = [AFHTTPSessionManager manager];
    [manager    GET:url.absoluteString
         parameters:nil
           progress:nil
            success:success
            failure:failure
     ];
}
    
@end
