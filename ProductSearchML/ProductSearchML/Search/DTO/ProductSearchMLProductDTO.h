//
//  ProductSearchMLProductDTO.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/MTLModel.h>
#import <Mantle/MTLJSONAdapter.h>

@interface ProductSearchMLProductDTO : MTLModel <MTLJSONSerializing>
    
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSString *thumbnail;
@property (nonatomic, copy) NSString *currencyId;
@property (nonatomic, copy) NSNumber *soldQuantity;
@property (nonatomic, copy) NSNumber *availableQuantity;
@property (nonatomic, copy) NSString *condition;
    
@end
