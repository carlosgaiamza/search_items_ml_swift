//
//  ProductSearchMLSearchPresenter.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import "ProductSearchMLSearchPresenter.h"
#import "ProductSearchMLSearchProvider.h"
#import "ProductSearchMLProductDTO.h"
#import "ProductSearchMLProductUITableViewCellDelegate.h"

@interface ProductSearchMLSearchPresenter ()
    
    @property (strong,nonatomic) ProductSearchMLSearchProvider *searchProvider;
    @property (weak,nonatomic) id<ProductSearchMLSearchMainViewDelegate> mainViewDelegate;
    
    @property (strong,nonatomic) NSArray *content;
    
@end

@implementation ProductSearchMLSearchPresenter
    
- (instancetype)initWithMainViewDelegate:(id<ProductSearchMLSearchMainViewDelegate>)delegate{
    self = [super init];

    if (self) {
        _mainViewDelegate = delegate;
        _searchProvider = [[ProductSearchMLSearchProvider alloc] init];
        _content = @[];
    }
    return self;
}
  
- (NSArray *)getProducts
{
        return self.content;
}
    
- (void)searchForProductsWithQuery:(NSString *)query
{
    
    if(query && [[query stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] != 0){
        
        [self.searchProvider searchProductWithQuery:query success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            NSError *error = nil;
            
            NSArray *products = [MTLJSONAdapter modelsOfClass:ProductSearchMLProductDTO.class fromJSONArray:responseObject[@"results"] error:&error];
            
            self.content = products;
            
            [self.mainViewDelegate reloadResults];
            
        }failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            self.content = @[];
            [self.mainViewDelegate reloadResults];
        }];
        
    }else{
        self.content = @[];
        [self.mainViewDelegate reloadResults];
    }
    
}

- (NSInteger)getResultsCount{
    return [self.content count];
}

- (void)navigateToProductDetail:(NSInteger)selectedRow{
    
    [self.mainViewDelegate navigateToProductDetails:[self.content objectAtIndex:selectedRow]];
}
    
- (void)renderCellWithRow:(NSInteger)selectedRow andCellViewDelegate:(id<ProductSearchMLProductUITableViewCellDelegate>) cellView{
    
    NSObject *item = [self.content objectAtIndex:selectedRow];

    ProductSearchMLProductDTO *product = (ProductSearchMLProductDTO *)item;
    
    [cellView setTitle:product.title
              andPrice:product.price
          andThumbnail:product.thumbnail];
}

@end
