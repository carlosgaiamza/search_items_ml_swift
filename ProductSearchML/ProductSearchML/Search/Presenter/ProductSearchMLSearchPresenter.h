//
//  ProductSearchMLSearchPresenter.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductSearchMLSearchMainViewDelegate.h"
#import "ProductSearchMLProductUITableViewCellDelegate.h"

@interface ProductSearchMLSearchPresenter : NSObject

- (instancetype)initWithMainViewDelegate:(id<ProductSearchMLSearchMainViewDelegate>)delegate;
    
- (void)searchForProductsWithQuery:(NSString *)query;

- (NSArray *)getProducts;
    
- (NSInteger)getResultsCount;
    
- (void)navigateToProductDetail:(NSInteger)selectedRow;
    
- (void)renderCellWithRow:(NSInteger)selectedRow andCellViewDelegate:(id<ProductSearchMLProductUITableViewCellDelegate>) cellView;

@end
