//
//  ProductSearchMLProductUITableViewCell.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//
#define MAS_SHORTHAND

#import "ProductSearchMLProductUITableViewCell.h"
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"

const int DEFAULT_MARGIN = 20;

@implementation ProductSearchMLProductUITableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self updateConstraints];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // configure control(s)
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont fontWithName:@"Arial" size:16.0f];
        [self addSubview:self.titleLabel];
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _priceLabel.textColor = [UIColor blackColor];
        _priceLabel.textAlignment = NSTextAlignmentRight;
        _priceLabel.font = [UIFont fontWithName:@"Arial" size:16.0f];
        [self addSubview:self.priceLabel];
        
        _customImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self addSubview:_customImageView];

    }
    
    return self;
}

// tell UIKit that you are using AutoLayout
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}
    
- (void)updateConstraints {
    
    [self.customImageView remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.contentView.mas_width).dividedBy(3);
        make.height.equalTo(self.contentView.mas_width).dividedBy(3);
        make.top.equalTo(self.mas_top);
        make.left.equalTo(self.mas_left);
    }];
    
    [self.titleLabel remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.left.equalTo(self.customImageView.mas_right).offset(DEFAULT_MARGIN);
        make.top.equalTo(self.mas_top).offset(DEFAULT_MARGIN);
        make.right.equalTo(self.mas_right).offset(-DEFAULT_MARGIN);
    }];
    
    [self.priceLabel remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(DEFAULT_MARGIN);
        make.right.equalTo(self.mas_right).offset(-DEFAULT_MARGIN);
    }];
    
    [super updateConstraints];
}
    
- (void)setTitle:(NSString *)title andPrice:(NSNumber *)price andThumbnail:(NSString *)thumbnail{
    
    self.titleLabel.text = title;
    self.priceLabel.text = [NSString stringWithFormat:@"$ %@", price];
    
    NSString *url1 = thumbnail;
    UIImage *placeholderImage = [UIImage imageNamed:@"no-product-image"];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url1]];
    
    __weak ProductSearchMLProductUITableViewCell *weakSelf = self;
    
    [self.customImageView setImageWithURLRequest:urlRequest placeholderImage:placeholderImage success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
       
        [weakSelf setNeedsLayout];
        weakSelf.customImageView.clipsToBounds = YES;
        weakSelf.customImageView.contentMode = UIViewContentModeScaleAspectFit;
        [weakSelf.customImageView setBounds:CGRectZero];
        [weakSelf.customImageView setImage:image];
        [weakSelf updateConstraints];
      
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {

        NSLog(@"%@", error);
    }];

}
    
@end
