//
//  ViewController.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductSearchMLSearchMainViewDelegate.h"

@interface ProductSearchMLSearchMainViewController : UIViewController<ProductSearchMLSearchMainViewDelegate>

@end

