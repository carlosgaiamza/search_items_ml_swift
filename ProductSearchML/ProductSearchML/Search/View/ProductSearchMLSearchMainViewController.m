//
//  ViewController.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import "ProductSearchMLSearchMainViewController.h"
#import "ProductSearchMLSearchPresenter.h"
#import "ProductSearchMLProductUITableViewCell.h"
#import "ProductSearchMLProductDetailViewController.h"

@interface ProductSearchMLSearchMainViewController () <UISearchResultsUpdating,UITableViewDelegate,UITableViewDataSource>
   
    @property (strong,nonatomic) UISearchController *searchController;
    @property (strong,nonatomic) UITableView *table;
    @property (strong,nonatomic) ProductSearchMLSearchPresenter *searchPresenter;
    
@end

@implementation ProductSearchMLSearchMainViewController
    
- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _searchPresenter = [[ProductSearchMLSearchPresenter alloc] initWithMainViewDelegate:self];
    
    [self configureTableview];
}
    
-(void)configureTableview{
    self.definesPresentationContext = YES;
    
    _table = [[UITableView alloc] initWithFrame:self.view.bounds
                                          style:UITableViewStyleGrouped];
    
    _table.delegate = self;
    _table.dataSource = self;
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    _searchController.hidesNavigationBarDuringPresentation = NO;
    _searchController.searchResultsUpdater = self;
    _searchController.dimsBackgroundDuringPresentation = NO;
    
    _table.tableHeaderView = _searchController.searchBar;
    
    _searchController.searchBar.barTintColor = [UIColor yellowColor];
    
    _searchController.searchBar.placeholder = @"Buscar en Mercado Libre";
    [self.view addSubview:self.table];
 
    UINavigationBar *bar = [self.navigationController navigationBar];
    
    [self.navigationItem setTitle:@"Busqueda de Productos"];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    [bar setBarTintColor:[UIColor yellowColor]];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {

    NSString *searchText = searchController.searchBar.text;
    
    [self.searchPresenter searchForProductsWithQuery:searchText];
}
    
- (void)reloadResults{
    [self.table reloadData];
}

- (void)navigateToProductDetails:(id)selectedProduct{
    
    ProductSearchMLProductDetailViewController *viewController = [[ProductSearchMLProductDetailViewController alloc] initWithProduct:selectedProduct];
    
    [self.navigationController pushViewController:viewController animated:true];
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellIdentifier = @"ProductCell";
    
    ProductSearchMLProductUITableViewCell *cell = (ProductSearchMLProductUITableViewCell *)[self.table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[ProductSearchMLProductUITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
   
    [self.searchPresenter renderCellWithRow:indexPath.row andCellViewDelegate:cell];
    
    return cell;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.searchPresenter navigateToProductDetail:indexPath.row];
    
    NSLog(@"title of cell %@", [[self.searchPresenter getProducts] objectAtIndex:indexPath.row]);
}
    
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
    
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.searchPresenter getResultsCount];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 158;
}
 
@end
