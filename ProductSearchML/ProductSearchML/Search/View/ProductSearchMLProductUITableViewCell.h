//
//  ProductSearchMLProductUITableViewCell.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductSearchMLProductUITableViewCellDelegate.h"

@interface ProductSearchMLProductUITableViewCell : UITableViewCell<ProductSearchMLProductUITableViewCellDelegate>
    
    @property (nonatomic, strong) UIImageView *customImageView;
    @property (nonatomic, strong) UILabel *titleLabel;
    @property (nonatomic, strong) UILabel *priceLabel;
    
@end
