//
//  ProductSearchMLProductUITableViewCellDelegate.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 15/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProductSearchMLProductUITableViewCellDelegate

- (void)setTitle:(NSString *)title andPrice:(NSNumber *)price andThumbnail:(NSString *)thumbnail;
    
@end
