//
//  ProductSearchMLSearchMainViewDelegate.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProductSearchMLSearchMainViewDelegate
    
- (void)reloadResults;
- (void)navigateToProductDetails:(id)selectedProduct;
    
@end
